package com.effecitech.process;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.compiere.model.MAttachment;
import org.compiere.model.MAttachmentEntry;
import org.compiere.model.PO;
import org.compiere.model.Query;
import org.compiere.process.SvrProcess;
import org.compiere.util.Env;

import com.effecitech.model.FC_MInvoice;
import com.effecitech.model.X_FCT_ExportLog;

public class ExportMultipleITAElectronicInvoice extends SvrProcess{

	@Override
	protected void prepare() {
		
	}

	@Override
	protected String doIt() throws Exception {
		String trxName = get_TrxName();
		
		/* Recupero le fatture selezionate*/
		String whereClause = "EXISTS (SELECT T_Selection_ID from T_Selection "
				+ " WHERE T_Selection.AD_PInstance_ID = ? " + " AND T_Selection.T_Selection_ID = C_Invoice.C_Invoice_ID)";

		List<PO> invoices = new Query(getCtx(), FC_MInvoice.Table_Name, whereClause, trxName).setClient_ID()
				.setParameters(new Object[] { getAD_PInstance_ID() }).list();
		
		/* Compongo il nome del file zip */
		SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
		String zipName = "Esportazione Fatture del " + format.format(new Timestamp(System.currentTimeMillis()))+".zip";
		
		/* Creo il file zip */
		File fileOut = new File(zipName);
		FileOutputStream fos = new FileOutputStream(fileOut);
		ZipOutputStream zipOut = new ZipOutputStream(fos);
		
		/*Scorro le fatture selezionate*/
		for(PO record: invoices) {
			
			FC_MInvoice invoice = new FC_MInvoice(getCtx(), record.get_ID(), trxName);
			
			/*Genero l'XML della Fattura Elettronica*/
			String result = invoice.exportInvoiceXML();
			if (result.startsWith("FILENAME")) {
				result = result.substring(8);
				
				/* Compongo il nome dell'XML*/
				String filename = invoice.getDocumentNo()+ "_" + format.format(invoice.getDateInvoiced()) +".xml";
				
				filename= filename.replaceAll("\\/", "_");
				
				/* Aggiungo il file XML allo zip*/
				FileInputStream fis = new FileInputStream(result);
				ZipEntry zipEntry = new ZipEntry(filename);
				zipOut.putNextEntry(zipEntry);
				
				byte[] bytes = new byte[1024];
	            int length;
	            while((length = fis.read(bytes)) >= 0) {
	                zipOut.write(bytes, 0, length);
	            }
	            fis.close();
	            
	            /* Inserisco sulla tabella dei log delle esportazioni l'XML*/
	            X_FCT_ExportLog exportLog = new X_FCT_ExportLog(Env.getCtx(), 0, trxName);
	            exportLog.setAD_Table_ID(FC_MInvoice.Table_ID);
	            exportLog.setRecord_ID(invoice.getC_Invoice_ID());
	            exportLog.setDescription("Esportazione Fattura Elettronica");
	            exportLog.saveEx();
	            
	            fis = new FileInputStream(result);
	            MAttachment attachment = exportLog.createAttachment();
	            MAttachmentEntry entry = new MAttachmentEntry(filename, fis.readAllBytes());
	            attachment.addEntry(entry);
	            attachment.saveEx(trxName);
	            fis.close();
	            
			}else {
				zipOut.close();
				throw new IllegalArgumentException("Fattura " + invoice.getDocumentNo()+ " : " + result);
			}
		}
		
		/* Mostro la finestra per il download del file*/
		processUI.download(fileOut);
		zipOut.close();
		fos.close();
		
		/* Inserisco sulla tabella dei log delle esportazioni lo ZIP */
        X_FCT_ExportLog exportLog = new X_FCT_ExportLog(Env.getCtx(), 0, trxName);
        exportLog.setAD_Table_ID(FC_MInvoice.Table_ID);
        exportLog.setRecord_ID(0);
        exportLog.setDescription("Esportazione Multipla Fatture Elettroniche");
        exportLog.saveEx();
        
        MAttachment attachment = exportLog.createAttachment();
        attachment.addEntry(fileOut);
        attachment.saveEx(trxName);
        
		return null;
	}

}
