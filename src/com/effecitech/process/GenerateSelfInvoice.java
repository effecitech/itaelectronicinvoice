package com.effecitech.process;

import org.adempiere.util.Callback;
import org.adempiere.webui.apps.AEnv;
import org.adempiere.webui.util.ServerPushTemplate;
import org.compiere.model.MDocType;
import org.compiere.model.MInvoice;
import org.compiere.model.MPeriod;
import org.compiere.model.MQuery;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Env;

import com.effecitech.window.CommonServerPushCallbackOpenWindow;

public class GenerateSelfInvoice extends SvrProcess {

	private int c_invoice_id = 0;

	@Override
	protected void prepare() {
		c_invoice_id = getRecord_ID();
	}

	@Override
	protected String doIt() throws Exception {
		String trxName = get_TrxName();

		MInvoice invoice = new MInvoice(Env.getCtx(), c_invoice_id, trxName);

		// Check if invoice is completed
		if (!invoice.getDocStatus().equals(MInvoice.DOCSTATUS_Completed)) {
			throw new IllegalArgumentException("La fattura non è completata");
		}

		MDocType doctype = new MDocType(Env.getCtx(), invoice.getC_DocType_ID(), trxName);

		// Test if self-invoice can be created from this doctype
		if (!doctype.get_ValueAsBoolean("IsCanSelfInvoiceBeGenerated")) {
			throw new UnsupportedOperationException(
					"Non è possibile creare un'autofattura per questo tipo di documento");
		}

		if (doctype.get_ValueAsInt("fct_selfinvoice_doctype_id") <= 0) {
			throw new IllegalArgumentException("Nessun tipo di documento per l'autofattura definito");
		}

		int fct_selfinvoice_doctype_id = doctype.get_ValueAsInt("fct_selfinvoice_doctype_id");
		MDocType selfInvoiceDoctype = new MDocType(Env.getCtx(), fct_selfinvoice_doctype_id, trxName);

		// Test periods
		MPeriod.testPeriodOpen(getCtx(), invoice.getDateAcct(), fct_selfinvoice_doctype_id, invoice.getAD_Org_ID());

		// Test if self-invoice already created
		String sql = "SELECT count(*) FROM c_invoice WHERE SelfInvoiceOrigin_ID = ?";
		int count = DB.getSQLValue(trxName, sql, invoice.getC_Invoice_ID());

		if (count > 0) {
			StringBuffer answer = new StringBuffer();

			processUI.ask("Sono già presenti una o più autofatture, procedere con la generazione?",
					new Callback<Boolean>() {
						@Override
						public void onCallback(Boolean result) {
							answer.append(result);
						}
					});

			while (answer.length() == 0) {
				try {
					Thread.sleep(200);
				} catch (InterruptedException e) {
				}
			}

			if (!Boolean.valueOf(answer.toString())) {
				return null;
			}
		}

		MInvoice selfInvoice = new MInvoice(Env.getCtx(), 0, trxName);
		selfInvoice.setAD_Org_ID(invoice.getAD_Org_ID());
		selfInvoice.setC_DocTypeTarget_ID(fct_selfinvoice_doctype_id);
		selfInvoice.setC_BPartner_ID(invoice.getC_BPartner_ID());
		selfInvoice.setC_BPartner_Location_ID(invoice.getC_BPartner_Location_ID());
		selfInvoice.setDateInvoiced(invoice.getDateAcct());
		selfInvoice.setIsSOTrx(true);
		selfInvoice.set_ValueOfColumn("fct_fedoctype_ID", selfInvoiceDoctype.get_Value("fct_fedoctype_ID"));
		selfInvoice.set_ValueOfColumn("SelfInvoiceOrigin_ID", invoice.getC_Invoice_ID());
		selfInvoice.saveEx(trxName);

		selfInvoice.copyLinesFrom(invoice, true, true);

		openInvoiceWindow(selfInvoice.getC_Invoice_ID());
		return "Creata autofattura " + selfInvoice.getDocumentNo();
	}

	private void openInvoiceWindow(int c_invoice_id) {
		String queryRestriction = " C_Invoice_ID = " + c_invoice_id;

		MQuery query = new MQuery("c_invoice");
		query.addRestriction(queryRestriction);
		query.setRecordCount(1);

		ServerPushTemplate pushUpdateUi = new ServerPushTemplate(AEnv.getDesktop());
		CommonServerPushCallbackOpenWindow callback = new CommonServerPushCallbackOpenWindow();
		callback.setParam(167, query, false);
		pushUpdateUi.executeAsync(callback);
	}

}
