/**
 * 
 */
package com.effecitech.process;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Level;

import org.adempiere.util.Callback;
import org.adempiere.webui.apps.AEnv;
import org.adempiere.webui.session.SessionManager;
import org.adempiere.webui.window.FDialog;
import org.apache.commons.codec.binary.Base64;

import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Env;

import com.effecitech.model.FC_MInvoice;
import com.effecitech.model.X_fct_bp_esenzione;

/**
 * @author cesca
 *
 */
public class ExportITAElectronicInvoice extends SvrProcess {

	private int m_AD_Client_ID = 0;
	private int recordID = 0;
	private boolean sendFile = false;

	@Override
	protected void prepare() {
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++) {
			String name = para[i].getParameterName();
			if (name.equals("modalita"))
				sendFile = "SYN".equals(para[i].getParameter().toString()); // HARDCODED: SYN = send, XML = only make
																			// xml file
			/*
			 * else if (name.equals("DeleteOldImported")) m_deleteOldImported =
			 * "Y".equals(para[i].getParameter());
			 */
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
		m_AD_Client_ID = getAD_Client_ID();
		recordID = getRecord_ID();
	}

	@Override
	protected String doIt() throws Exception {
		FC_MInvoice invoice = new FC_MInvoice(getCtx(), recordID, get_TrxName());

		int fct_BP_Esenzione_ID = invoice.get_ValueAsInt("FCT_BP_Esenzione_ID");
		if (fct_BP_Esenzione_ID > 0) {
			X_fct_bp_esenzione esenzione = new X_fct_bp_esenzione(Env.getCtx(), fct_BP_Esenzione_ID, get_TrxName());

			String sql = "SELECT calcolaPlafondUsato(?) FROM dual";
			BigDecimal plafondUsato = DB.getSQLValueBD(get_TrxName(), sql, fct_BP_Esenzione_ID);

			if (plafondUsato.compareTo(esenzione.getPlafond()) > 0) {
				final StringBuffer proceed = new StringBuffer();

				processUI.ask(
						"Importo Dichiarazione d'Intento splafondato, continuare con la creazione della Fattura Elettronica?",
						new Callback<Boolean>() {

							@Override
							public void onCallback(Boolean result) {
								proceed.append(result);
							}
						});
				
				while(proceed.toString().isBlank()) {
					Thread.sleep(500);
				}
				
				if(proceed.toString().toLowerCase().equals("false")) {
					return null;
				}
			}
		}

		String result = invoice.exportInvoiceXML();

		if (result.startsWith("FILENAME")) {

			result = result.substring(8);

			if (processUI != null) {
				final StringBuffer answer = new StringBuffer();
				// processUI.ask("asadas", null);
				// processUI.statusUpdate("prova update message");
				// process answer from user
				processUI.download(new File(result));
				// processUI.download(new File("ciao pippo"));
			}

			byte[] encoded = Files.readAllBytes(Paths.get(result));
			String content = new String(encoded, StandardCharsets.UTF_8);
			System.out.println(content);
			byte[] byteArray = Base64.encodeBase64(content.getBytes());

			DB.executeUpdate("update c_invoice set xmlfile=?, isToUpload='Y',xmldate=now() where c_invoice_id=?",
					new Object[] { byteArray, invoice.getC_Invoice_ID() }, false, get_TrxName());

			if (result.length() > 10) {
				result = result.substring(result.length() - 15);
			}

			int ibanidx = content.indexOf("IBAN");

			if (ibanidx > 0) {
				String ibanFromXML = content.substring(ibanidx + 23, ibanidx + 32);

				result += " CONTO:" + ibanFromXML;
			} else {
				String ibanFromXML = "NON VALORIZZATO";
				// processUI.download(new File(ibanFromXML));
				result += " CONTO:" + ibanFromXML;
			}

			return result;
		}
		return "File non creato - " + result;
	}

}
