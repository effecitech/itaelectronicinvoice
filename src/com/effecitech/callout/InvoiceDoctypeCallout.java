package com.effecitech.callout;

import java.util.Properties;

import org.adempiere.base.IColumnCallout;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.model.MDocType;
import org.compiere.model.MInvoice;
import org.compiere.util.Env;

public class InvoiceDoctypeCallout implements IColumnCallout {
	@Override
	public String start(Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value, Object oldValue) {

		if(!mTab.getValueAsBoolean(MInvoice.COLUMNNAME_IsSOTrx)) {
			return null;
		}		
		
		/* Recupero il tipo di documento */
		int c_doctypetarget_id = Integer.parseInt(value.toString());

		MDocType doctype = new MDocType(Env.getCtx(), c_doctypetarget_id, null);

		/* Recupero se presente il Tipo Documento Fattura Elettronica */
		int fct_fedoctype_ID = doctype.get_ValueAsInt("fct_fedoctype_ID");

		if (fct_fedoctype_ID <= 0) {
			return "Nessun Tipo Documento Fattura Elettronica definita per : " + doctype.getName();
		}
		
		mTab.setValue("fct_fedoctype_ID", fct_fedoctype_ID);

		return null;
	}
}
