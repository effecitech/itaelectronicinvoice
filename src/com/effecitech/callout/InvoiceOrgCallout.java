package com.effecitech.callout;

import java.util.Properties;

import org.adempiere.base.IColumnCallout;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.util.DB;

public class InvoiceOrgCallout implements IColumnCallout {

	@Override
	public String start(Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value, Object oldValue) {
		if(value == null) {
			return null;
		}
		
		int ad_Org_ID = (int) value;
		
		String sql = "SELECT RiferimentoAmministrazione FROM FC_OrgInfo WHERE AD_Org_ID = ?";
		
		String riferimentoAmministrazione = DB.getSQLValueString(null, sql, ad_Org_ID);
		
        mTab.setValue("RiferimentoAmministrazione", riferimentoAmministrazione);
		return null;
	}

}
