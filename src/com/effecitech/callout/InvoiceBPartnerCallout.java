package com.effecitech.callout;

import java.sql.Timestamp;
import java.util.Properties;

import org.adempiere.base.IColumnCallout;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.model.MInvoice;
import org.compiere.util.DB;

import com.effecitech.model.X_fct_bp_esenzione;

public class InvoiceBPartnerCallout implements IColumnCallout{

	@Override
	public String start(Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value, Object oldValue) {
		if(value == null) {
			mTab.setValue("fct_BP_Esenzione_ID", null);
			return null;
		}
		
		int c_BPartner_ID = 0;
		
		try {
			c_BPartner_ID = Integer.parseInt(value.toString());
		}catch(Exception e) {
			c_BPartner_ID = 0;
		}
		
		if(c_BPartner_ID <= 0) {
			mTab.setValue("fct_BP_Esenzione_ID", null);
			return null;
		}
		
		Timestamp dateInvoiced = (Timestamp) mTab.getValue(MInvoice.COLUMNNAME_DateInvoiced);
		
		String sql = "SELECT Fct_BP_Esenzione_ID FROM Fct_BP_Esenzione WHERE C_BPartner_ID = ? "
				+ " AND IsActive = 'Y' AND DateRegistered <= ?"
				+ " ORDER BY DateRegistered DESC"
				+ " LIMIT 1";
		
		int fct_BP_Esenzione_ID = DB.getSQLValue(null, sql, c_BPartner_ID, dateInvoiced);

		mTab.setValue("fct_BP_Esenzione_ID", fct_BP_Esenzione_ID);
		if(fct_BP_Esenzione_ID > 0) {
			X_fct_bp_esenzione esenzione = new X_fct_bp_esenzione(ctx, fct_BP_Esenzione_ID, null);
			
			if(esenzione.getPlafondRimanente().signum() <= 0) {
				return "Dichiarazione d'Intento splafondata";
			}
		}
		
		return null;
	}

}
