package com.effecitech.callout;

import java.util.Properties;

import org.adempiere.base.IColumnCallout;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.model.MInvoice;
import org.compiere.model.MPaymentTerm;
import org.compiere.util.Env;

public class InvoicePaymentTermCallout implements IColumnCallout{

	@Override
	public String start(Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value, Object oldValue) {
		
		if(!mTab.getValueAsBoolean(MInvoice.COLUMNNAME_IsSOTrx)) {
			return null;
		}
		
		/*Recupero il termine di pagamento*/
		int c_paymentterm_id = Integer.parseInt(value.toString());
		
		MPaymentTerm term = new MPaymentTerm(Env.getCtx(),c_paymentterm_id,null);
		
		/*Recupero se presente la Modalità del pagamento per la Fattura Elettronica*/
		String einvoicePaymentTerm = term.get_ValueAsString("FC_ModalitaPagamento");
		
		if(einvoicePaymentTerm == null || einvoicePaymentTerm.isBlank()) {
			return "Nessuna Modalità di Pagamento definita per : " + term.getName();
		}
		
		mTab.setValue("FC_ModalitaPagamento", einvoicePaymentTerm);
		
		return null;
	}

}
