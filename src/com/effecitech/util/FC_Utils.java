/**
 * 
 */
package com.effecitech.util;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Locale;


/**
 * @author cesca
 *
 */
public class FC_Utils {
	
	/**
	 * Get document or current date in format - default yyyy-MM-dd
	 * @param docDate a Timestamp of the document date or null for current date
	 * @param format int value can be 10 for date like '12/01/2018' or 29 for complete date like '2012-10-22T16:46:12.000+02:00'. Other values gets "yyyy-MM-dd"
	 * @return a String of the date formatted
	 */
	public static String getDate(Timestamp docDate, int format) {

		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
		if (docDate == null) docDate = new Timestamp(System.currentTimeMillis());
		
		if (format == 10)
			dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		else if (format == 29)
			dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
		
		return dateFormat.format(docDate);
	}
	
	/**
	 * 	Return a String with the same char count times
	 * 	@return string
	 */
	public static String fillString(int count, char fill) {
	    if (count < 0)
	    	return "";
		char[] s = new char[count];
	    Arrays.fill(s, fill);
	    return new String(s);
	}

	/**
	 * 	Return a trimmed String cut at length value
	 * 	@return string
	 */
	public static String cutString(String str, int length) {
		if (length < str.length())
			return new String(str.substring(0, length).trim());

		return new String(str.trim());
	}

	
	/**
	 * @param taxID a string of the tax id
	 * @return a string with only digits of the tax id 
	 * @throws FC_EmptyTaxIDException if empty or wrong string given
	 */
	public static String getTaxID(String taxID) throws FC_ElectronicInvoiceException {
		// check if empty
		if (taxID == null || taxID.trim().length() == 0)
			throw new FC_ElectronicInvoiceException("Errore: campo Partita iva non valorizzato. Controllare Informazioni Organizzazione e/o Business Partner.");
		// remove all characters
		//taxID = taxID.replaceAll("[^\\d]",""); //le p.iva straniere possono avere dei caratteri alfanumerici
		// remove white spaces
		taxID = taxID.replaceAll("\\s", "");
		int length = taxID.length();
		// TaxID MAX length is 28 
		if (length >0 && length <=28 )
			return taxID;
		else throw new FC_ElectronicInvoiceException("Errore: campo Partita iva errato. Controllare Informazioni Organizzazione e/o Business Partner.");
	}

	public static String getReceiverCode(String format, Object code, String pec) throws FC_ElectronicInvoiceException {
		if (code == null  || code.toString().trim().length() == 0)
			throw new FC_ElectronicInvoiceException("Errore: campo Codice Destinatario non valorizzato. Controllare i dati di fatturazione elettronica del Business Partner.");
		String cod = code.toString().trim();
		if ("FPR12".equals(format)) { // private bp
			
			if (cod.equals("0000000")) {
				if (pec != null && pec.length() > 6) // if code is '0000000' pec address must have more than 6 chars
					return cod;
				else throw new FC_ElectronicInvoiceException("Errore: campo Codice Destinatario pari a '0000000' ma indirizzo pec mancante o errato o Codice fiscale non impostato. Controllare i dati di fatturazione elettronica del Business Partner.");
			} else {
				if (cod.length() == 7) 
					return cod;
				else throw new FC_ElectronicInvoiceException("Errore: campo Codice Destinatario errato, per i privati deve avere 7 caratteri. Controllare i dati di fatturazione elettronica del Business Partner.");
			}
		} else { // PA
			if (cod.length() == 6) 
				return cod;
			else throw new FC_ElectronicInvoiceException("Errore: campo Codice Destinatario errato, per le PA deve avere 6 caratteri. Controllare i dati di fatturazione elettronica del Business Partner.");
		} 			
	}

	public static String formatNumber(BigDecimal number, String pattern) {
		
		NumberFormat format = new DecimalFormat(pattern);
		return format.format(number).replace(',','.'); 
	}
}

 class FC_ElectronicInvoiceException extends Exception {


	/**
	 * 
	 */
	private static final long serialVersionUID = 6495215835784730044L;
	private String msg = null;
	
	
	public String getMsg() {
		return msg;
	}


	public void setMsg(String msg) {
		this.msg = msg;
	}

	public FC_ElectronicInvoiceException(String error) {
		msg = error;
	}

	@Override
	public String getLocalizedMessage() {
		if (msg != null)
			return msg;
		return "Errore non gestito:" + super.getLocalizedMessage();
	}
	
}
