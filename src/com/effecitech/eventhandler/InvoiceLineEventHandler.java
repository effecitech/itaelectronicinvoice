package com.effecitech.eventhandler;

import java.math.BigDecimal;

import org.adempiere.base.event.AbstractEventHandler;
import org.compiere.model.MInvoice;
import org.compiere.model.MInvoiceLine;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.osgi.service.event.Event;

import com.effecitech.model.X_fct_bp_esenzione;

public class InvoiceLineEventHandler extends AbstractEventHandler {
	
	@Override
	protected void initialize() {
		//registerTableEvent(IEventTopics.PO_AFTER_NEW, MInvoiceLine.Table_Name);
		//registerTableEvent(IEventTopics.PO_AFTER_CHANGE, MInvoiceLine.Table_Name);
	}

	@Override
	protected void doHandleEvent(Event event) {
		if(!(getPO(event) instanceof MInvoiceLine)) {
			return;
		}
		
		MInvoiceLine invoiceLine = (MInvoiceLine) getPO(event);
		
		MInvoice invoice = (MInvoice) invoiceLine.getC_Invoice();
		
		int fct_BP_Esenzione_ID = invoice.get_ValueAsInt("FCT_BP_Esenzione_ID");
		
		if(fct_BP_Esenzione_ID <= 0 || !invoice.isSOTrx() /*|| !invoice.getDocBaseType().equalsIgnoreCase("ARI")*/) {
			return;
		}
		
		X_fct_bp_esenzione esenzione = new X_fct_bp_esenzione(Env.getCtx(), fct_BP_Esenzione_ID, invoiceLine.get_TrxName());
		
		String sql = "SELECT calcolaPlafondUsato(?) FROM dual";
		BigDecimal plafondUsato = DB.getSQLValueBD(invoiceLine.get_TrxName(), sql, fct_BP_Esenzione_ID);
		
		/*if(plafondUsato.compareTo(esenzione.getPlafond()) > 0) {
			//FDialog.warn(0, "Importo Dichiarazione d'Intento splafondato");
		}*/
	}
	
	private void handleNew(MInvoiceLine invoiceLine) {

	}

	private void handleChange(MInvoiceLine invoiceLine) {

	}
}
