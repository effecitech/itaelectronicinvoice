package com.effecitech.window;

import org.adempiere.util.Callback;
import org.adempiere.webui.adwindow.ADTabpanel;
import org.adempiere.webui.adwindow.ADWindow;
import org.adempiere.webui.session.SessionManager;
import org.adempiere.webui.util.IServerPushCallback;
import org.compiere.model.MQuery;

public class CommonServerPushCallbackOpenForm implements IServerPushCallback {


	 private int m_form_ID = 0;


	 public void setParam(int m_form_ID) {
		 this.m_form_ID = m_form_ID;
	 }


	 @Override
	 public void updateUI() {
		 SessionManager.getAppDesktop().openForm(m_form_ID);
	 }
	}

