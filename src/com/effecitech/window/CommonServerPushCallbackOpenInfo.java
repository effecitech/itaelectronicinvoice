package com.effecitech.window;

import org.adempiere.util.Callback;
import org.adempiere.webui.adwindow.ADTabpanel;
import org.adempiere.webui.adwindow.ADWindow;
import org.adempiere.webui.session.SessionManager;
import org.adempiere.webui.util.IServerPushCallback;
import org.compiere.model.MQuery;

public class CommonServerPushCallbackOpenInfo implements IServerPushCallback {


	 private int m_infoID = 0;
	 //private MQuery m_query = null;
	 //private boolean m_isNew = false;


	 public void setParam(int infoID) {
		 m_infoID = infoID;
		 //m_query = query;
		 //m_isNew = isNew;
	 }


	 @Override
	 public void updateUI() {
		 SessionManager.getAppDesktop().openInfo(m_infoID);
	 }
	}

