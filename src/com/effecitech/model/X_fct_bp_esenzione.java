/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.effecitech.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;

/** Generated Model for fct_bp_esenzione
 *  @author iDempiere (generated) 
 *  @version Release 10 - $Id$ */
//@org.adempiere.base.Model(table="fct_bp_esenzione")
public class X_fct_bp_esenzione extends PO implements I_fct_bp_esenzione, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20240222L;

    /** Standard Constructor */
    public X_fct_bp_esenzione (Properties ctx, int fct_bp_esenzione_ID, String trxName)
    {
      super (ctx, fct_bp_esenzione_ID, trxName);
      /** if (fct_bp_esenzione_ID == 0)
        {
			setC_BPartner_ID (0);
			setDateRegistered (new Timestamp( System.currentTimeMillis() ));
			setfct_bp_esenzione_ID (0);
			setNumeroProtocollo (null);
			setPlafond (Env.ZERO);
// 0
        } */
    }

    /** Standard Constructor */
    /*public X_fct_bp_esenzione (Properties ctx, int fct_bp_esenzione_ID, String trxName, String ... virtualColumns)
    {
      super (ctx, fct_bp_esenzione_ID, trxName, virtualColumns);
    }*/

    /** Load Constructor */
    public X_fct_bp_esenzione (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuilder sb = new StringBuilder ("X_fct_bp_esenzione[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_C_BPartner getC_BPartner() throws RuntimeException
	{
		return (org.compiere.model.I_C_BPartner)MTable.get(getCtx(), org.compiere.model.I_C_BPartner.Table_ID)
			.getPO(getC_BPartner_ID(), get_TrxName());
	}

	/** Set Business Partner .
		@param C_BPartner_ID Identifies a Business Partner
	*/
	public void setC_BPartner_ID (int C_BPartner_ID)
	{
		if (C_BPartner_ID < 1)
			set_ValueNoCheck (COLUMNNAME_C_BPartner_ID, null);
		else
			set_ValueNoCheck (COLUMNNAME_C_BPartner_ID, Integer.valueOf(C_BPartner_ID));
	}

	/** Get Business Partner .
		@return Identifies a Business Partner
	  */
	public int getC_BPartner_ID()
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_BPartner_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_Tax getC_Tax() throws RuntimeException
	{
		return (org.compiere.model.I_C_Tax)MTable.get(getCtx(), org.compiere.model.I_C_Tax.Table_ID)
			.getPO(getC_Tax_ID(), get_TrxName());
	}

	/** Set Tax.
		@param C_Tax_ID Tax identifier
	*/
	public void setC_Tax_ID (int C_Tax_ID)
	{
		if (C_Tax_ID < 1)
			set_Value (COLUMNNAME_C_Tax_ID, null);
		else
			set_Value (COLUMNNAME_C_Tax_ID, Integer.valueOf(C_Tax_ID));
	}

	/** Get Tax.
		@return Tax identifier
	  */
	public int getC_Tax_ID()
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Tax_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Date Registered.
		@param DateRegistered Date Registered
	*/
	public void setDateRegistered (Timestamp DateRegistered)
	{
		set_Value (COLUMNNAME_DateRegistered, DateRegistered);
	}

	/** Get Date Registered.
		@return Date Registered	  */
	public Timestamp getDateRegistered()
	{
		return (Timestamp)get_Value(COLUMNNAME_DateRegistered);
	}

    /** Get Record ID/ColumnName
        @return ID/ColumnName pair
      */
    public KeyNamePair getKeyNamePair() 
    {
        return new KeyNamePair(get_ID(), String.valueOf(getDateRegistered()));
    }

	/** Set Esenzione Business Partner.
		@param fct_bp_esenzione_ID Esenzione Business Partner
	*/
	public void setfct_bp_esenzione_ID (int fct_bp_esenzione_ID)
	{
		if (fct_bp_esenzione_ID < 1)
			set_ValueNoCheck (COLUMNNAME_fct_bp_esenzione_ID, null);
		else
			set_ValueNoCheck (COLUMNNAME_fct_bp_esenzione_ID, Integer.valueOf(fct_bp_esenzione_ID));
	}

	/** Get Esenzione Business Partner.
		@return Esenzione Business Partner	  */
	public int getfct_bp_esenzione_ID()
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_fct_bp_esenzione_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set fct_bp_esenzione_UU.
		@param fct_bp_esenzione_UU fct_bp_esenzione_UU
	*/
	public void setfct_bp_esenzione_UU (String fct_bp_esenzione_UU)
	{
		set_ValueNoCheck (COLUMNNAME_fct_bp_esenzione_UU, fct_bp_esenzione_UU);
	}

	/** Get fct_bp_esenzione_UU.
		@return fct_bp_esenzione_UU	  */
	public String getfct_bp_esenzione_UU()
	{
		return (String)get_Value(COLUMNNAME_fct_bp_esenzione_UU);
	}

	/** Set Numero Protocollo.
		@param NumeroProtocollo Numero Protocollo
	*/
	public void setNumeroProtocollo (String NumeroProtocollo)
	{
		set_Value (COLUMNNAME_NumeroProtocollo, NumeroProtocollo);
	}

	/** Get Numero Protocollo.
		@return Numero Protocollo	  */
	public String getNumeroProtocollo()
	{
		return (String)get_Value(COLUMNNAME_NumeroProtocollo);
	}

	/** Set Plafond.
		@param Plafond Plafond
	*/
	public void setPlafond (BigDecimal Plafond)
	{
		set_Value (COLUMNNAME_Plafond, Plafond);
	}

	/** Get Plafond.
		@return Plafond	  */
	public BigDecimal getPlafond()
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Plafond);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Plafond Rimanente.
		@param PlafondRimanente Plafond Rimanente
	*/
	public void setPlafondRimanente (BigDecimal PlafondRimanente)
	{
		throw new IllegalArgumentException ("PlafondRimanente is virtual column");	}

	/** Get Plafond Rimanente.
		@return Plafond Rimanente	  */
	public BigDecimal getPlafondRimanente()
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_PlafondRimanente);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}
}