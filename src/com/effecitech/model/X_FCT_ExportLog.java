/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.effecitech.model;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;

/** Generated Model for FCT_ExportLog
 *  @author iDempiere (generated) 
 *  @version Release 10 - $Id$ */
//@org.adempiere.base.Model(table="FCT_ExportLog")
public class X_FCT_ExportLog extends PO implements I_FCT_ExportLog, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20240301L;

    /** Standard Constructor */
    public X_FCT_ExportLog (Properties ctx, int FCT_ExportLog_ID, String trxName)
    {
      super (ctx, FCT_ExportLog_ID, trxName);
      /** if (FCT_ExportLog_ID == 0)
        {
			setAD_Table_ID (0);
			setExportedDate (new Timestamp( System.currentTimeMillis() ));
// SYSDATE
			setFCT_ExportLog_ID (0);
			setRecord_ID (0);
        } */
    }

    /** Standard Constructor */
    /*public X_FCT_ExportLog (Properties ctx, int FCT_ExportLog_ID, String trxName, String ... virtualColumns)
    {
      super (ctx, FCT_ExportLog_ID, trxName, virtualColumns);
    }*/

    /** Load Constructor */
    public X_FCT_ExportLog (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuilder sb = new StringBuilder ("X_FCT_ExportLog[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_AD_Table getAD_Table() throws RuntimeException
	{
		return (org.compiere.model.I_AD_Table)MTable.get(getCtx(), org.compiere.model.I_AD_Table.Table_ID)
			.getPO(getAD_Table_ID(), get_TrxName());
	}

	/** Set Table.
		@param AD_Table_ID Database Table information
	*/
	public void setAD_Table_ID (int AD_Table_ID)
	{
		if (AD_Table_ID < 1)
			set_Value (COLUMNNAME_AD_Table_ID, null);
		else
			set_Value (COLUMNNAME_AD_Table_ID, Integer.valueOf(AD_Table_ID));
	}

	/** Get Table.
		@return Database Table information
	  */
	public int getAD_Table_ID()
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_AD_Table_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Description.
		@param Description Optional short description of the record
	*/
	public void setDescription (String Description)
	{
		set_ValueNoCheck (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription()
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Exported Date.
		@param ExportedDate Exported Date
	*/
	public void setExportedDate (Timestamp ExportedDate)
	{
		set_ValueNoCheck (COLUMNNAME_ExportedDate, ExportedDate);
	}

	/** Get Exported Date.
		@return Exported Date	  */
	public Timestamp getExportedDate()
	{
		return (Timestamp)get_Value(COLUMNNAME_ExportedDate);
	}

	/** Set Log di Esportazione.
		@param FCT_ExportLog_ID Log di Esportazione
	*/
	public void setFCT_ExportLog_ID (int FCT_ExportLog_ID)
	{
		if (FCT_ExportLog_ID < 1)
			set_ValueNoCheck (COLUMNNAME_FCT_ExportLog_ID, null);
		else
			set_ValueNoCheck (COLUMNNAME_FCT_ExportLog_ID, Integer.valueOf(FCT_ExportLog_ID));
	}

	/** Get Log di Esportazione.
		@return Log di Esportazione	  */
	public int getFCT_ExportLog_ID()
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_FCT_ExportLog_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set FCT_ExportLog_UU.
		@param FCT_ExportLog_UU FCT_ExportLog_UU
	*/
	public void setFCT_ExportLog_UU (String FCT_ExportLog_UU)
	{
		set_ValueNoCheck (COLUMNNAME_FCT_ExportLog_UU, FCT_ExportLog_UU);
	}

	/** Get FCT_ExportLog_UU.
		@return FCT_ExportLog_UU	  */
	public String getFCT_ExportLog_UU()
	{
		return (String)get_Value(COLUMNNAME_FCT_ExportLog_UU);
	}

	/** Set Record ID.
		@param Record_ID Direct internal record ID
	*/
	public void setRecord_ID (int Record_ID)
	{
		if (Record_ID < 0)
			set_ValueNoCheck (COLUMNNAME_Record_ID, null);
		else
			set_ValueNoCheck (COLUMNNAME_Record_ID, Integer.valueOf(Record_ID));
	}

	/** Get Record ID.
		@return Direct internal record ID
	  */
	public int getRecord_ID()
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Record_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}