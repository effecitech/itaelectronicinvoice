/**
 *
 * https://bitbucket.org/CarlosRuiz_globalqss/facturacionelectronica
 * http://www.fatturapa.gov.it/export/fatturazione/sdi/fatturapa/v1.2/IT01234567890_FPR02.xml
 *
 */
package com.effecitech.model;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.sax.SAXTransformerFactory;
import javax.xml.transform.sax.TransformerHandler;
import javax.xml.transform.stream.StreamResult;

import com.effecitech.util.FC_Utils;

import org.compiere.model.MBPartner;
import org.compiere.model.MCharge;
import org.compiere.model.MDocType;
import org.compiere.model.MInvoice;
import org.compiere.model.MInvoiceLine;
import org.compiere.model.MInvoicePaySchedule;
import org.compiere.model.MLocation;
import org.compiere.model.MOrder;
import org.compiere.model.MOrderLine;
import org.compiere.model.MOrgInfo;
import org.compiere.model.MSysConfig;
import org.compiere.model.MTax;
import org.compiere.util.AdempiereUserError;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.xml.sax.helpers.AttributesImpl;

/**
 * @author cesca
 *
 */
public class FC_MInvoice extends MInvoice {

	public FC_MInvoice(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1233223976644763704L;

	public FC_MInvoice(Properties ctx, int C_Invoice_ID, String trxName) {
		super(ctx, C_Invoice_ID, trxName);
	}

	private String file_name = "";

	public String exportInvoiceXML() {
		OutputStream  mmDocStream = null;
		String trxName = get_TrxName();
		
		String codiceDestinatario = new String();
		boolean autoFattura = false;

		try {
			String doctype = "";
			/* <TipoDocumento>		
			TD01		fattura
			TD02		acconto/anticipo su fattura
			TD03		acconto/anticipo su parcella
			TD04		nota di credito
			TD05		nota di debito
			TD06		parcella */	
			int dtID = getC_DocType_ID() == 0? getC_DocTypeTarget_ID(): getC_DocType_ID();
			MDocType dt = new MDocType(getCtx(), dtID, trxName);
			if (dt.getDocBaseType().equals(MDocType.DOCBASETYPE_ARInvoice) || 
					dt.getDocBaseType().equals(MDocType.DOCBASETYPE_APInvoice)	)
				doctype = "TD01";
			if (dt.getDocBaseType().equals(MDocType.DOCBASETYPE_ARCreditMemo))
				doctype = "TD04";
			if (dt.getDocBaseType().equals(MDocType.DOCBASETYPE_APCreditMemo))
				doctype = "TD05";
			
			
			int fct_fedoctype_ID = get_ValueAsInt("fct_fedoctype_ID");
			if (fct_fedoctype_ID > 0) {
				doctype = DB.getSQLValueString(trxName, "select fetypecode from fct_fedoctype where fct_fedoctype_ID = ? ", fct_fedoctype_ID);
			}
			
			if (doctype.equals("TD16") || doctype.equals("TD17") || doctype.equals("TD18")) {
				autoFattura = true;
			}

			// vendor
			MOrgInfo oi = MOrgInfo.get(getCtx(), getAD_Org_ID(), trxName);
			MLocation lo = new MLocation(getCtx(), oi.getC_Location_ID(), trxName);
			String v_prov = DB.getSQLValueString(trxName, "select locode from c_city where c_city_id=?", lo.getC_City_ID());
			
			String v_countrycode = DB.getSQLValueString(trxName, 
					"SELECT c_country.countrycode FROM c_city inner join c_country on c_country.c_country_id = c_city.c_country_id "
					+ "WHERE c_city_id=?", lo.getC_City_ID());
			
			if (v_prov == null || v_prov.trim().length() != 2 ) // prov must have 2 chars
				throw new Exception("Errore: verificare la provincia dell'organizzazione.");
			v_prov = v_prov.trim();

			String v_taxid = FC_Utils.getTaxID(oi.getTaxID());

			String vendorSql = "SELECT name, provinciauffregimpr, numerorea, capitalesociale, riferimentoamministrazione, "
					+ " regimefiscale, sociounico, statoliquidazione, fc_pathfilexml, fiscalcode, fc_tiporitenuta, "
					+ " fc_tipocassa, codicedestinatario, indirizzopec,forzainclusionedatiordine "
					+ " FROM fc_orginfo WHERE ad_org_id=?";
			
			List<Object> vendorResult = DB.getSQLValueObjectsEx(trxName, vendorSql, getAD_Org_ID());
			String v_fisccode = vendorResult.get(9)+"";

			String cig = DB.getSQLValueString(trxName, "select cig from c_invoice where c_invoice_id=?", getC_Invoice_ID()); // CIG 
			String cup = DB.getSQLValueString(trxName, "select cup from c_invoice where c_invoice_id=?", getC_Invoice_ID()); // CUP
			String fct_CodiceCommessaConvenzione = DB.getSQLValueString(trxName, 
					"select fct_CodiceCommessaConvenzione from c_invoice where c_invoice_id=?", getC_Invoice_ID());
			boolean forzaInclusioneDatiOrdine = vendorResult.get(14).toString().equals("Y");
			String riferimentoAmministrazione = get_ValueAsString("RiferimentoAmministrazione");

			// customer
			MBPartner bp = new MBPartner(getCtx(), getC_BPartner_ID(), trxName);
			String c_fiscalcode = DB.getSQLValueString(trxName, "select coalesce(fiscalcode, '') from c_bpartner where c_bpartner_id=?", getC_BPartner_ID());
			String customerSql = "select is_PA, name, codicedestinatario, indirizzopec from fc_bp_einvoice where c_bpartner_id=?";
			List<Object> customerResult = DB.getSQLValueObjectsEx(trxName, customerSql, getC_BPartner_ID());
			String format = "";
			if (customerResult == null)
				throw new Exception("Errore: compilare la tab della fatturazione elettronica per il Business Partner "+bp.getName());
			String pec = (String)customerResult.get(3);
			
			if (customerResult != null && "N".equals(customerResult.get(0))) // not PA
				format = "FPR12";
			else format = "FPA12";
			int c_loc_id = DB.getSQLValue(trxName, "select c_location_id from c_bpartner_location where c_bpartner_location_id=?", getC_BPartner_Location_ID());
			if (c_loc_id <= 0)
				throw new Exception("Errore: Verificare l'indirizzo del bp.");
			MLocation c_loc = new MLocation(getCtx(), c_loc_id,trxName);
			String c_prov = c_loc.get_ValueAsString("locode");// DB.getSQLValueString(trxName, "select locode from c_city where c_city_id=?", c_loc.getC_City_ID());
			
			if (c_prov == null || c_prov.isEmpty()) {
				c_prov = DB.getSQLValueString(trxName, "select locode from c_city where c_city_id=?", c_loc.getC_City_ID());
			}
			
			String c_countrycode = DB.getSQLValueString(trxName, 
					"SELECT c_country.countrycode FROM c_country WHERE c_country_id = ?", c_loc.getC_Country_ID());
			
			if (c_countrycode != null && c_countrycode.equals("IT")) {
				if (c_prov == null || c_prov.trim().length() != 2 ) // prov must have 2 chars
					throw new Exception("Errore: verificare la provincia dell'indirizzo del Cliente.");
				c_prov = c_prov.trim();
			}
			
			String receiverCode = new String();
			
			if (!autoFattura) {
				String code = customerResult.get(2)+"";
				if ("0000000".equals(code) && c_fiscalcode != null && !c_fiscalcode.isEmpty() && /*(bp.getTaxID() == null || bp.getTaxID().isEmpty()) && */ (pec == null || pec.isEmpty()))
					pec = "***NOTNECESSARY***";
				receiverCode = FC_Utils.getReceiverCode(format, code, pec);
			} else {
				String code = vendorResult.get(12)+"";
				String v_pec = vendorResult.get(13)+"";
				if ("0000000".equals(code) && v_fisccode != null && !v_fisccode.isEmpty() &&  (v_pec == null || v_pec.isEmpty()))
					v_pec = "***NOTNECESSARY***";
				receiverCode = FC_Utils.getReceiverCode(format, code, v_pec);
			}
				
			
			String id = this.get_ID()+"";
			String xmlFileName = v_countrycode + v_taxid +"_"+ id.substring(id.length()-5,id.length()) +".xml";
			
			/*Controllo che sia presente la dichiarazione di intento se almeno una riga ha natura N3.5*/
			for(MInvoiceLine line : getLines()) {
				MTax tax = new MTax(Env.getCtx(),line.getC_Tax_ID(),trxName);
				if(tax.get_ValueAsString("Natura").equals("N3.5") && this.get_ValueAsInt("fct_bp_esenzione_id")<=0) {
					throw new Exception("Errore: Compilare il campo esenzione");
				}
			}

			// complete path to xml 
			//file_name = vendorResult.get(8) + File.separator + xmlFileName;
			file_name = "/tmp" + File.separator + xmlFileName;
			//Stream for the xml document
			mmDocStream = new FileOutputStream (file_name, false);
			StreamResult streamResult_menu = new StreamResult(new OutputStreamWriter(mmDocStream,"UTF-8"));
			SAXTransformerFactory tf_menu = (SAXTransformerFactory) SAXTransformerFactory.newInstance();					
			tf_menu.setAttribute("indent-number", new Integer(0));

			TransformerHandler mmDoc = tf_menu.newTransformerHandler();	
			Transformer serializer_menu = mmDoc.getTransformer();	
			serializer_menu.setOutputProperty(OutputKeys.METHOD,"xml");
			serializer_menu.setOutputProperty(OutputKeys.ENCODING,"UTF-8");
			serializer_menu.setOutputProperty(OutputKeys.INDENT,"yes");

			mmDoc.setResult(streamResult_menu);

			mmDoc.startDocument();

			AttributesImpl atts = new AttributesImpl();

			// header
			atts.clear();

			atts.clear();
			atts.addAttribute("", "", "versione", "CDATA", format);
			atts.addAttribute("", "", "xmlns:ds", "CDATA", "http://www.w3.org/2000/09/xmldsig#");
			atts.addAttribute("", "", "xmlns:p", "CDATA", "http://ivaservizi.agenziaentrate.gov.it/docs/xsd/fatture/v1.2");
			atts.addAttribute("", "", "xmlns:xsi", "CDATA", "http://www.w3.org/2001/XMLSchema-instance");
			atts.addAttribute("", "", "xsi:schemaLocation", "CDATA", "http://ivaservizi.agenziaentrate.gov.it/docs/xsd/fatture/v1.2 http://www.fatturapa.gov.it/export/fatturazione/sdi/fatturapa/v1.2/Schema_del_file_xml_FatturaPA_versione_1.2.xsd");
			mmDoc.startElement("", "", "p:FatturaElettronica", atts);

			atts.clear();

			// transmitter
			mmDoc.startElement("","","FatturaElettronicaHeader", atts);
			mmDoc.startElement("","","DatiTrasmissione",atts);
			mmDoc.startElement("","","IdTrasmittente",atts);

			addHeaderElement(mmDoc, "IdPaese", v_countrycode, atts);
			addHeaderElement(mmDoc, "IdCodice", v_fisccode, atts);

			mmDoc.endElement("", "", "IdTrasmittente");

			addHeaderElement(mmDoc, "ProgressivoInvio", getC_Invoice_ID() + "", atts);
			addHeaderElement(mmDoc, "FormatoTrasmissione", format, atts);
			addHeaderElement(mmDoc, "CodiceDestinatario", receiverCode, atts);
			if ("0000000".equals(receiverCode) && !"***NOTNECESSARY***".equals(pec))
				addHeaderElement(mmDoc, "PECDestinatario", pec.trim(), atts);

			mmDoc.endElement("", "", "DatiTrasmissione");
			
			
			//CedentePrestatore
			
			CedentePrestatore cedentePrestatore = new CedentePrestatore();
			if (!autoFattura) {
				cedentePrestatore.setIdPaese(v_countrycode);
				cedentePrestatore.setIdCodice(oi.getTaxID());
				cedentePrestatore.setCodiceFiscale(v_fisccode);
				cedentePrestatore.setDenominazione(vendorResult.get(0).toString());
				cedentePrestatore.setRegimeFiscale(vendorResult.get(5).toString());
				cedentePrestatore.setIndirizzo(lo.getAddress1());
				cedentePrestatore.setCap(lo.getPostal());
				cedentePrestatore.setComune(lo.getCity());
				cedentePrestatore.setProvincia(v_prov);
				cedentePrestatore.setNazione(v_countrycode);
				cedentePrestatore.setRiferimentoAmministrazione(riferimentoAmministrazione);
			} else {
				cedentePrestatore.setIdPaese(c_countrycode);
				cedentePrestatore.setIdCodice(bp.getTaxID());
				cedentePrestatore.setDenominazione(customerResult.get(1).toString());
				cedentePrestatore.setRegimeFiscale("RF01");
				cedentePrestatore.setIndirizzo(c_loc.getAddress1());
				cedentePrestatore.setCap(c_loc.getPostal());
				cedentePrestatore.setComune(c_loc.getCity());
				cedentePrestatore.setProvincia(c_prov);
				cedentePrestatore.setNazione(c_countrycode);
				cedentePrestatore.setRiferimentoAmministrazione(riferimentoAmministrazione);
			}
			
			mmDoc.startElement("","","CedentePrestatore", atts);
			mmDoc.startElement("","","DatiAnagrafici", atts);
			mmDoc.startElement("","","IdFiscaleIVA", atts);

			addHeaderElement(mmDoc, "IdPaese", cedentePrestatore.getIdPaese(), atts);
			addHeaderElement(mmDoc, "IdCodice", FC_Utils.getTaxID(cedentePrestatore.getIdCodice()), atts);
			//addHeaderElement(mmDoc, "CodiceFiscale", FC_Utils.getTaxID(cedentePrestatore.getCodiceFiscale()), atts);

			mmDoc.endElement("", "", "IdFiscaleIVA");
			mmDoc.startElement("","","Anagrafica", atts);
			addHeaderElement(mmDoc, "Denominazione", cedentePrestatore.getDenominazione(), atts); // name
			mmDoc.endElement("", "", "Anagrafica");
			addHeaderElement(mmDoc, "RegimeFiscale", cedentePrestatore.getRegimeFiscale(), atts); 
			mmDoc.endElement("", "", "DatiAnagrafici");
			mmDoc.startElement("","","Sede", atts);
			addHeaderElement(mmDoc, "Indirizzo", cedentePrestatore.getIndirizzo(), atts);
			if (lo.getPostal() == null || lo.getPostal().trim().length() != 5)
				throw new Exception ("Errore: verificare cap organizzazione.");
			addHeaderElement(mmDoc, "CAP", cedentePrestatore.getCap(), atts);
			addHeaderElement(mmDoc, "Comune", cedentePrestatore.getComune(), atts);
			//addHeaderElement(mmDoc, "Provincia", cedentePrestatore.getProvincia(), atts); 
			if (cedentePrestatore.getIdPaese()  != null && cedentePrestatore.getIdPaese().equals("IT") && cedentePrestatore.getProvincia() !=null) {
				addHeaderElement(mmDoc, "Provincia", cedentePrestatore.getProvincia(), atts);
			}
			addHeaderElement(mmDoc, "Nazione", cedentePrestatore.getNazione(), atts);
			mmDoc.endElement("", "", "Sede");
			if(cedentePrestatore.getRiferimentoAmministrazione() != null) {
				addHeaderElement(mmDoc, "RiferimentoAmministrazione", cedentePrestatore.getRiferimentoAmministrazione(), atts);
			}
			mmDoc.endElement("", "", "CedentePrestatore");


			//CessionarioCommittente
			
			CessionarioCommittente cessionarioCommittente = new CessionarioCommittente();
			if (!autoFattura) {
				cessionarioCommittente.setIdPaese(c_countrycode);
				cessionarioCommittente.setIdCodice(bp.getTaxID());
				cessionarioCommittente.setDenominazione(customerResult.get(1).toString());
				cessionarioCommittente.setCodiceFiscale(c_fiscalcode.trim());
				cessionarioCommittente.setIndirizzo(c_loc.getAddress1());
				cessionarioCommittente.setCap(c_loc.getPostal());
				cessionarioCommittente.setComune(c_loc.getCity());
				cessionarioCommittente.setProvincia(c_prov);
				cessionarioCommittente.setNazione(c_countrycode);
			} else {
				cessionarioCommittente.setIdPaese(v_countrycode);
				cessionarioCommittente.setIdCodice(oi.getTaxID());
				cessionarioCommittente.setDenominazione(vendorResult.get(0).toString());
				cessionarioCommittente.setCodiceFiscale(v_fisccode);
				cessionarioCommittente.setIndirizzo(lo.getAddress1());
				cessionarioCommittente.setCap(lo.getPostal());
				cessionarioCommittente.setComune(lo.getCity());
				cessionarioCommittente.setProvincia(v_prov);
				cessionarioCommittente.setNazione(v_countrycode);
			} 
	
			mmDoc.startElement("","","CessionarioCommittente", atts);
			mmDoc.startElement("","","DatiAnagrafici", atts);
			if ((bp.getTaxID() == null || bp.getTaxID().isEmpty()) && (c_fiscalcode == null || c_fiscalcode.isEmpty()))
				throw new Exception("Errore: compilare almeno uno dei campi partita iva o codice fiscale per il bp "+bp.getName());
			if (bp.getTaxID() != null && !bp.getTaxID().isEmpty()) {
				mmDoc.startElement("","","IdFiscaleIVA", atts);

				addHeaderElement(mmDoc, "IdPaese", cessionarioCommittente.getIdPaese(), atts);  
				addHeaderElement(mmDoc, "IdCodice", FC_Utils.getTaxID(cessionarioCommittente.getIdCodice()), atts);

				mmDoc.endElement("", "", "IdFiscaleIVA");
			}
			if (c_fiscalcode != null && c_fiscalcode.trim().length()>0)
				addHeaderElement(mmDoc, "CodiceFiscale", cessionarioCommittente.getCodiceFiscale(), atts);
			mmDoc.startElement("","","Anagrafica", atts);
			addHeaderElement(mmDoc, "Denominazione", cessionarioCommittente.getDenominazione(), atts);
			mmDoc.endElement("", "", "Anagrafica");
			mmDoc.endElement("", "", "DatiAnagrafici");
			mmDoc.startElement("","","Sede", atts);
			addHeaderElement(mmDoc, "Indirizzo", cessionarioCommittente.getIndirizzo(), atts);
			if (cessionarioCommittente.getIdPaese() != null && cessionarioCommittente.getIdPaese().equals("IT")) {
				if (cessionarioCommittente.getCap() == null || cessionarioCommittente.getCap().trim().length() != 5)
					throw new Exception ("Errore: verificare cap bp.");
			}
			
			if (cessionarioCommittente.getCap() == null)
				throw new Exception("Errore: Compilare il campo CAP");
			
			addHeaderElement(mmDoc, "CAP", cessionarioCommittente.getCap(), atts);
			addHeaderElement(mmDoc, "Comune", cessionarioCommittente.getComune(), atts);
			if (cessionarioCommittente.getIdPaese()  != null && cessionarioCommittente.getIdPaese().equals("IT") && cessionarioCommittente.getProvincia() !=null) {
				addHeaderElement(mmDoc, "Provincia", cessionarioCommittente.getProvincia(), atts);
			}
			addHeaderElement(mmDoc, "Nazione", cessionarioCommittente.getNazione(), atts); 
			mmDoc.endElement("", "", "Sede");
			mmDoc.endElement("", "", "CessionarioCommittente");

			mmDoc.endElement("", "", "FatturaElettronicaHeader");

			// Body
			mmDoc.startElement("","","FatturaElettronicaBody", atts);
			mmDoc.startElement("","","DatiGenerali", atts);	
			mmDoc.startElement("","","DatiGeneraliDocumento", atts);	
			addHeaderElement(mmDoc, "TipoDocumento", doctype, atts);
			addHeaderElement(mmDoc, "Divisa", "EUR", atts);
			addHeaderElement(mmDoc, "Data", FC_Utils.getDate(this.getDateInvoiced(), 0), atts);
			addHeaderElement(mmDoc, "Numero", getDocumentNo(), atts);

			BigDecimal importoTotaleDocumento = getGrandTotal();
			
			
			String dutySql = "select sum(coalesce(il.linenetamt,0)) "+ 
					"from c_invoiceline il "+ 
					"join c_tax t on (il.c_tax_id=t.c_tax_id and t.istaxexempt='Y' and t.isdutystamp='Y') " +  
					"where (il.c_charge_id is null or il.c_charge_id<>?) and il.c_invoice_id=?";

			// check if dutystamp is necessary 
			int dutyID = MSysConfig.getIntValue("DutyStamp_Charge_ID", -1, getAD_Client_ID(), getAD_Org_ID());
			BigDecimal dutyTotalNetAmountLimit = new BigDecimal(MSysConfig.getDoubleValue("DutyStamp_TotalNetAmountLimit", -1, getAD_Client_ID(), getAD_Org_ID()));
			/*
			if (dutyID > 0) {
				BigDecimal totalLineDuty = DB.getSQLValueBD(trxName, dutySql, dutyID, getC_Invoice_ID());
				if (totalLineDuty!= null && totalLineDuty.compareTo(dutyTotalNetAmountLimit) >=0) {
					dutySql = "select c_charge_id from c_invoiceline where c_invoice_id=" + getC_Invoice_ID() + " and c_charge_id=?";
					int idDuty = DB.getSQLValue(trxName, dutySql, dutyID);
					if (idDuty != dutyID)
						throw new Exception("Errore: manca il bollo, verificare le righe fattura.");
				} else {
					dutyID = -1;
				}
			}
			*/
			boolean dutyNeeded = false;
			boolean dutyInserted = false;
			String dutySqlCheck = "select sum(coalesce(il.linenetamt,0)) "+ 
					"from c_invoiceline il "+ 
					"join c_tax t on (il.c_tax_id=t.c_tax_id and t.istaxexempt='Y' and t.isdutystamp='Y') " +  
					"where il.c_invoice_id=?";
			BigDecimal totalLineDutyCheck = DB.getSQLValueBD(trxName, dutySqlCheck, getC_Invoice_ID());
			if (totalLineDutyCheck != null && totalLineDutyCheck != Env.ZERO && totalLineDutyCheck.compareTo(dutyTotalNetAmountLimit) > 0) {
				dutyNeeded = true;
			}
			
			// duty stamp
			if (dutyID > 0){
				
				String bolloACaricoDelClienteSql = "select isDutyStampCharged from c_bpartner where c_bpartner_id=" + getC_BPartner_ID();
				String bolloACaricoDelCliente = DB.getSQLValueString(trxName, bolloACaricoDelClienteSql);
				
				String dutyIdSql = "select c_charge_id from c_invoiceline where c_invoice_id=" + getC_Invoice_ID() + " and c_charge_id=?";
				int idDuty = DB.getSQLValue(trxName, dutyIdSql, dutyID);
				
				MDocType docType = new MDocType(getCtx(), getC_DocTypeTarget_ID(), get_TrxName());
				if (dutyNeeded && idDuty != dutyID && bolloACaricoDelCliente.equals("Y") && !docType.getDocBaseType().equals("ARC"))
					throw new Exception("Errore: manca il bollo, verificare le righe fattura.");
				
				BigDecimal totalLineDuty = DB.getSQLValueBD(trxName, dutySql, dutyID, getC_Invoice_ID());
				
				if (totalLineDuty!= null && totalLineDuty.compareTo(dutyTotalNetAmountLimit) >=0) {
					MCharge dutyStamp = new MCharge(getCtx(), dutyID, trxName);
					mmDoc.startElement("","","DatiBollo", atts);
					addHeaderElement(mmDoc, "BolloVirtuale", "SI", atts);
					addHeaderElement(mmDoc, "ImportoBollo", FC_Utils.formatNumber(dutyStamp.getChargeAmt(),"#0.00"), atts); 
					mmDoc.endElement("", "", "DatiBollo");
					dutyInserted = true;
				}  
				
				/*
				MCharge dutyStamp = new MCharge(getCtx(), dutyID, trxName);
				mmDoc.startElement("","","DatiBollo", atts);
				addHeaderElement(mmDoc, "BolloVirtuale", "SI", atts);
				addHeaderElement(mmDoc, "ImportoBollo", FC_Utils.formatNumber(dutyStamp.getChargeAmt(),"#0.00"), atts); 
				mmDoc.endElement("", "", "DatiBollo");
				*/
			}
			
			if (dutyNeeded && !dutyInserted) {
				throw new Exception("Errore: Bollo previsto ma non presente in fattura.");
			}

			// withholding
			String withSql = "select max(c_tax_id) from c_tax where iswithholding='Y' and parent_tax_id IN (select distinct c_tax_id from c_invoiceline where c_invoice_id=?)";
			int withTaxID = DB.getSQLValue(trxName, withSql, getC_Invoice_ID());
			if (withTaxID>0 && !MInvoice.DOCSTATUS_Completed.equals(getDocStatus())) 
				throw new Exception("Errore: Completare la fattura per calcolare importi ritenuta.");
			else if (withTaxID>0) {
				if (vendorResult.get(10) == null)
					throw new Exception("Errore: Impostare Tipo Ritenuta in Organizzazione.");
				String causale = DB.getSQLValueString(trxName, "select fc_paymentreason from c_invoice where c_invoice_id=?", getC_Invoice_ID()); // CUP
				if (causale == null)
					throw new Exception("Errore: Impostare Causale Pagamento in Fattura.");
				BigDecimal withAmt = DB.getSQLValueBD(trxName, "select -1*(sum(coalesce(taxamt,0))) from c_invoicetax where c_invoice_id=? "
						+ "	and c_tax_id in (select c_tax_id from c_tax where iswithholding='Y')", getC_Invoice_ID());
				if (withAmt == null || withAmt.equals(BigDecimal.ZERO))
					throw new Exception("Errore: Verificare calcoli ritenuta.");
				BigDecimal rate = DB.getSQLValueBD(trxName, "select rate from c_tax where c_tax_id=?", withTaxID);
				importoTotaleDocumento = importoTotaleDocumento.add(withAmt);
				// we assume there is just one withholding for one invoice.	
				mmDoc.startElement("","","DatiRitenuta", atts);
				addHeaderElement(mmDoc, "TipoRitenuta", vendorResult.get(10)+"", atts);
				addHeaderElement(mmDoc, "ImportoRitenuta", FC_Utils.formatNumber(withAmt,"#0.00"), atts);
				addHeaderElement(mmDoc, "AliquotaRitenuta", FC_Utils.formatNumber(rate.abs(),"#0.00"), atts);
				addHeaderElement(mmDoc, "CausalePagamento", causale, atts);
				mmDoc.endElement("", "", "DatiRitenuta");
			}

			// slitPayment
			String spSql = "select max(c_tax_id) from c_tax where issplitpayment='Y' and parent_tax_id IN (select distinct c_tax_id from c_invoiceline where c_invoice_id=?)";
			int spTax_ID = DB.getSQLValue(trxName, spSql, getC_Invoice_ID());
			if (spTax_ID>0 && !MInvoice.DOCSTATUS_Completed.equals(getDocStatus())) 
				throw new Exception("Errore: Completare la fattura per calcolare importi splitpayment.");
			else if (spTax_ID>0) {
				BigDecimal spAmt = DB.getSQLValueBD(trxName, "select -1*(sum(coalesce(taxamt,0))) from c_invoicetax where c_invoice_id=? "
						+ "	and c_tax_id in (select c_tax_id from c_tax where issplitpayment='Y')", getC_Invoice_ID());
				if (spAmt == null || spAmt.equals(BigDecimal.ZERO))
					throw new Exception("Errore: Verificare calcoli splitpayment.");
				//BigDecimal rate = DB.getSQLValueBD(trxName, "select rate from c_tax where c_tax_id=?", spTax_ID);
				importoTotaleDocumento = importoTotaleDocumento.add(spAmt);
			}

			String contributiSql = "select sum(il.linenetamt), t.rate, case when il.c_tax_id in (select parent_tax_id from c_tax where iswithholding='Y') then 'SI' else NULL end as ritenuta,t.natura,t.istaxexempt,c.rate "
					+ " from c_invoiceline il left join c_tax t on (il.c_tax_id=t.c_tax_id) inner join c_charge c on (il.c_charge_id =c.c_charge_id and c.issocialsecurity='Y') " + 
					" where il.c_invoice_id=? " + 
					" group by c.rate,t.rate, il.c_tax_id,t.natura,t.istaxexempt ";
			
			List<List<Object>> contributi = DB.getSQLArrayObjectsEx(trxName, contributiSql, getC_Invoice_ID());
			List<Object> riga = null;
			if (contributi != null && contributi.size()>0)
				if (vendorResult.get(11) == null || ((String)vendorResult.get(11)).length()!=4)
					throw new Exception("Errore: Impostare tipo cassa in Organizzazione.");
				else	{
					String imponibileCassaSql = "select coalesce(sum(il.linenetamt), 0) " + 
							"from c_invoiceline il where c_invoice_id=? and c_charge_id is null and c_tax_id not in "
							+ "(select c_tax_id from c_tax where istaxexempt='Y')";
					BigDecimal imponibileCassa = DB.getSQLValueBD(trxName, imponibileCassaSql, getC_Invoice_ID());
					for (int i=0;i<contributi.size();i++) {
						riga=contributi.get(i);
						if (riga.get(5) == null)
							throw new Exception("Errore: Impostare aliquota % contributo previdenziale.");
						mmDoc.startElement("","","DatiCassaPrevidenziale", atts);
						addHeaderElement(mmDoc, "TipoCassa", vendorResult.get(11)+"", atts);
						addHeaderElement(mmDoc, "AlCassa", FC_Utils.formatNumber((BigDecimal)riga.get(5),"#0.00"), atts);
						addHeaderElement(mmDoc, "ImportoContributoCassa", FC_Utils.formatNumber((BigDecimal)riga.get(0),"#0.00"), atts);
						addHeaderElement(mmDoc, "ImponibileCassa", FC_Utils.formatNumber(imponibileCassa,"#0.00"), atts);
						addHeaderElement(mmDoc, "AliquotaIVA", FC_Utils.formatNumber((BigDecimal)riga.get(1),"#0.00"), atts);
						if ("SI".equals(riga.get(2)))
							addHeaderElement(mmDoc, "Ritenuta", "SI", atts);
						if ("Y".equals(riga.get(4))) { // is tax exempt
							if (riga.get(3) == null)
								throw new Exception("Errore: Impostare natura dell'imposta.");
							addHeaderElement(mmDoc, "Natura", riga.get(3)+"", atts);
						}
						mmDoc.endElement("", "", "DatiCassaPrevidenziale");
					}
				}
			addHeaderElement(mmDoc, "ImportoTotaleDocumento", FC_Utils.formatNumber(importoTotaleDocumento,"#0.00"), atts);
			
			String causale = "";
			if (getDescription() != null && getDescription().trim().length() > 0)
				causale = FC_Utils.cutString(getDescription(), 200);
			
			//casusale 2.1.1.11
			String causaleSql = "select coalesce(i.FECausale, bp.FECausale, '') from C_invoice i "
					+ " inner join c_bpartner bp on bp.c_bpartner_id = i.c_bpartner_id "
					+ " where i.c_invoice_id=?";
			if (!causale.equals("") && causale != null)
				causale += " ";
			
			causale += DB.getSQLValueString(trxName, causaleSql, getC_Invoice_ID());
			
			if (!causale.equals("") && causale != null) {
				addHeaderElement(mmDoc, "Causale", FC_Utils.cutString(causale, 200), atts);
			}
			
			mmDoc.endElement("", "", "DatiGeneraliDocumento");
			boolean noOrder = true;
			// Check in the invoice lines if there are connections with the order lines	
			MInvoiceLine[] lines = getLines();	
			if (lines == null || lines.length == 0)
				throw new Exception ("Errore: inserire le righe fattura.");
			for (int i =0; i<lines.length; i++) {
				MInvoiceLine line = lines[i];
				String updateLine = "update c_invoiceline set xmlline="+(i+1)+" where c_invoiceline_id="+line.getC_InvoiceLine_ID();
				DB.executeUpdate(updateLine, trxName);

				/*if (line.getC_OrderLine_ID() > 0) {
			noOrder = false;
			MOrderLine oline = new MOrderLine(getCtx(), line.getC_OrderLine_ID(), trxName);
			MOrder order = new MOrder(getCtx(), oline.getC_Order_ID(), trxName);  
			mmDoc.startElement("","","DatiOrdineAcquisto", atts); // it's written "acquisto" but it's a sales order for the vendor, purchase order for the PA
			//int xmlline = DB.getSQLValue(trxName, "select xmlline from c_invoiceline where c_invoiceline_id=?", line.getC_InvoiceLine_ID());
			//	addHeaderElement(mmDoc, "RiferimentoNumeroLinea", xmlline+"", atts); //was line.getLine()
				addHeaderElement(mmDoc, "IdDocumento", order.getDocumentNo(), atts);
				addHeaderElement(mmDoc, "Data", FC_Utils.getDate(order.getDateOrdered(), 0), atts);
				addHeaderElement(mmDoc, "NumItem", oline.getLine()+"", atts);
			  if (cup != null)
				addHeaderElement(mmDoc, "CodiceCUP", cup, atts);
			  if (cig != null)
				addHeaderElement(mmDoc, "CodiceCIG", cig, atts);
			mmDoc.endElement("", "", "DatiOrdineAcquisto");
			}*/
			}
			
			if ((noOrder && (cig != null || cup != null)) || (forzaInclusioneDatiOrdine && getPOReference() != null && getPOReference().trim().length() > 0)) {
				mmDoc.startElement("","","DatiOrdineAcquisto", atts); // it's written "acquisto" but it's a sales order for the vendor, purchase order for the PA
				//addHeaderElement(mmDoc, "RiferimentoNumeroLinea", "1", atts);
				
				//ignoreRifOrderWarningCUPCIG se impostato a Y ignore this warning
				boolean ignoreMissingRifOrderCUPCIG_Error = MSysConfig.getBooleanValue("IgnoreMissingRifOrderCUPCIG_Error", false, getAD_Client_ID(), getAD_Org_ID());
				//ignoreRifOrderWarningCUPCIG
				boolean ignoreMissingDateOrderCUPCIG_Error = MSysConfig.getBooleanValue("IgnoreMissingDateOrderCUPCIG_Error", false, getAD_Client_ID(), getAD_Org_ID());
				
				if (getPOReference() == null || getPOReference().isEmpty()) {
					if (!ignoreMissingRifOrderCUPCIG_Error)
						throw new Exception("Errore: Se CIG o CUP valorizzati, impostare Rif. Ordine.");
				} else {
					addHeaderElement(mmDoc, "IdDocumento", getPOReference(), atts);
				}
				
				if (getDateOrdered() == null) {
					if (!ignoreMissingDateOrderCUPCIG_Error)
						throw new Exception("Errore: Se CIG o CUP valorizzati, impostare Data Ordine.");
				} else {	
					addHeaderElement(mmDoc, "Data", FC_Utils.getDate(getDateOrdered(), 0), atts);
				}
				
				//addHeaderElement(mmDoc, "NumItem", "1", atts);
				if (cup != null)
					addHeaderElement(mmDoc, "CodiceCUP", cup, atts);
				if (cig != null)
					addHeaderElement(mmDoc, "CodiceCIG", cig, atts);
				if (fct_CodiceCommessaConvenzione != null)
					addHeaderElement(mmDoc, "CodiceCommessaConvenzione", fct_CodiceCommessaConvenzione, atts);
				
				mmDoc.endElement("", "", "DatiOrdineAcquisto");
			}
			
			// search for DatiOrdineAcquisto
			String ordCollegati ="select distinct fct_datiordineacquisto_id from fct_datiordineacquisto where c_invoice_id=?";
			List<Object> datiOrdCollIDs = DB.getSQLValueObjectsEx(trxName, ordCollegati, getC_Invoice_ID());
			if (datiOrdCollIDs != null && datiOrdCollIDs.size() > 0) {
				for (int i=0;i<datiOrdCollIDs.size();i++) {
					String datiFattCollSql = "select fc.idDocumento, fc.dataDocumento , fc.numItem, fc.riferimentoNumeroLinea, " +
							"fc.codiceCommessaConvenzione,  fc.codiceCUP, fc.codiceCIG " +
							"from fct_datiordineacquisto fc " + 
							"where fc.fct_datiordineacquisto_id = ?";
					List<Object> datiOrdineColl = DB.getSQLValueObjectsEx(trxName, datiFattCollSql, datiOrdCollIDs.get(i));
					mmDoc.startElement("","","DatiOrdineAcquisto", atts);
					addHeaderElement(mmDoc, "RiferimentoNumeroLinea", datiOrdineColl.get(3)+"", atts);
					addHeaderElement(mmDoc, "IdDocumento", datiOrdineColl.get(0)+"", atts);
					addHeaderElement(mmDoc, "Data", FC_Utils.getDate((Timestamp)datiOrdineColl.get(1), 0), atts);
					if (datiOrdineColl.get(2) != null && !((String)datiOrdineColl.get(2)).isEmpty())
					addHeaderElement(mmDoc, "NumItem", datiOrdineColl.get(2)+"", atts);
					if (datiOrdineColl.get(4) != null && !((String)datiOrdineColl.get(4)).isEmpty())
					addHeaderElement(mmDoc, "CodiceCommessaConvenzione", datiOrdineColl.get(4)+"", atts);
					if (datiOrdineColl.get(5) != null && !((String)datiOrdineColl.get(5)).isEmpty())
					addHeaderElement(mmDoc, "CodiceCUP", datiOrdineColl.get(5)+"", atts);
					if (datiOrdineColl.get(6) != null && !((String)datiOrdineColl.get(6)).isEmpty())
					addHeaderElement(mmDoc, "CodiceCIG", datiOrdineColl.get(6)+"", atts);
	
					mmDoc.endElement("", "", "DatiOrdineAcquisto");
				}
			}
			
			// search for fattura collegate
			String invCollegate ="select distinct fct_datifatturecollegate_id from fct_datifatturecollegate where c_invoice_id=?";
			List<Object> datiFattCollIDs = DB.getSQLValueObjectsEx(trxName, invCollegate, getC_Invoice_ID());
			
			boolean unicoRifFatture = false;
			
			if (datiFattCollIDs != null && datiFattCollIDs.size() > 0) {
				for (int i=0;i<datiFattCollIDs.size();i++) {
					String datiFattCollSql = "select fc.idDocumento, fc.dataDocumento , fc.numItem, fc.riferimentoNumeroLinea, " +
							"fc.codiceCommessaConvenzione,  fc.codiceCUP, fc.codiceCIG " +
							"from fct_datifatturecollegate fc " + 
							"where fc.fct_datifatturecollegate_id = ?";
					List<Object> datiFattColl = DB.getSQLValueObjectsEx(trxName, datiFattCollSql, datiFattCollIDs.get(i));
					mmDoc.startElement("","","DatiFattureCollegate", atts);
					if (datiFattColl.get(3) != null) {
						addHeaderElement(mmDoc, "RiferimentoNumeroLinea", datiFattColl.get(3)+"", atts);
						unicoRifFatture = true;
					}
					addHeaderElement(mmDoc, "IdDocumento", datiFattColl.get(0)+"", atts);
					addHeaderElement(mmDoc, "Data", FC_Utils.getDate((Timestamp)datiFattColl.get(1), 0), atts);
					if (datiFattColl.get(2) != null && !((String)datiFattColl.get(2)).isEmpty())
					addHeaderElement(mmDoc, "NumItem", datiFattColl.get(2)+"", atts);
					if (datiFattColl.get(4) != null && !((String)datiFattColl.get(4)).isEmpty())
					addHeaderElement(mmDoc, "CodiceCommessaConvenzione", datiFattColl.get(4)+"", atts);
					if (datiFattColl.get(5) != null && !((String)datiFattColl.get(5)).isEmpty())
					addHeaderElement(mmDoc, "CodiceCUP", datiFattColl.get(5)+"", atts);
					if (datiFattColl.get(6) != null && !((String)datiFattColl.get(6)).isEmpty())
					addHeaderElement(mmDoc, "CodiceCIG", datiFattColl.get(6)+"", atts);
	
					mmDoc.endElement("", "", "DatiFattureCollegate");
				}
				
				if (datiFattCollIDs.size() > 1 && unicoRifFatture) {
					throw new Exception("RiferimentoNumeroLinea su DatiFattureCollegate non valorizzato correttamente");
				}
				
			}
			
			// search for inout
			String ddtSql ="select distinct m_inout_id from m_inoutline where m_inoutline_id in (select m_inoutline_id from c_invoiceline where c_invoice_id=? group by c_invoice_id,m_inoutline_id,xmlline order by xmlline)";
			//List<Object> ddtID = DB.getSQLValueObjectsEx(trxName, ddtSql, getC_Invoice_ID());
			int [] ddtID = DB.getIDsEx(trxName, ddtSql, getC_Invoice_ID());
			if (ddtID != null && ddtID.length > 0) {
				for (int i=0;i<ddtID.length;i++) {
					ddtSql = "select d.documentno, d.dateAcct,il.xmlline from m_inout d " + 
							"join m_inoutline dl on (d.m_inout_id=dl.m_inout_id) " + 
							"left join c_invoiceline il on (il.m_inoutline_id=dl.m_inoutline_id) " + 
							"where d.m_inout_id = ?";
					List<Object> ddt = DB.getSQLValueObjectsEx(trxName, ddtSql, ddtID[i]);
					mmDoc.startElement("","","DatiDDT", atts);
					addHeaderElement(mmDoc, "NumeroDDT", ddt.get(0)+"", atts);
					addHeaderElement(mmDoc, "DataDDT", FC_Utils.getDate((Timestamp)ddt.get(1), 0), atts);
					if (ddt.get(2) != null)
						addHeaderElement(mmDoc, "RiferimentoNumeroLinea", ddt.get(2)+"", atts);
					mmDoc.endElement("", "", "DatiDDT");
				}
			}
			/*
			 * 
<DatiTrasporto>
<DatiAnagraficiVettore>
<IdFiscaleIVA>
<IdPaese>IT</IdPaese>
<IdCodice>24681012141</IdCodice>
</IdFiscaleIVA>
<Anagrafica>
<Denominazione>Trasporto spa</Denominazione>
</Anagrafica>
</DatiAnagraficiVettore>
<DataOraConsegna>2012-10-22T16:46:12.000+02:00</DataOraConsegna>
</DatiTrasporto>
			 * 
			 * */

			mmDoc.endElement("", "", "DatiGenerali");
			mmDoc.startElement("","","DatiBeniServizi", atts);
			boolean isDescr = false;
			boolean found22 = false;
			BigDecimal twentytwo = new BigDecimal("22");

			for (int i =0; i<lines.length; i++) {
				MInvoiceLine line = lines[i];
				if (line.getC_Charge_ID()>0) {
					String cassaSql = "select issocialsecurity from c_charge where c_charge_id=?";
					if ("Y".equals(DB.getSQLValueString(trxName, cassaSql, line.getC_Charge_ID())))
						continue;
				}
				MTax tax = new MTax(getCtx(), line.getC_Tax_ID(), trxName);
				mmDoc.startElement("","","DettaglioLinee", atts);
				int xmlline = DB.getSQLValue(trxName, "select xmlline from c_invoiceline where c_invoiceline_id=?", line.getC_InvoiceLine_ID());
				addHeaderElement(mmDoc, "NumeroLinea", xmlline +"", atts); //line.getLine()
				
				if (line.getM_Product_ID() > 0) {
					//aggiungo la parte del prodotto
					mmDoc.startElement("","","CodiceArticolo", atts);
						addHeaderElement(mmDoc, "CodiceTipo", FC_Utils.cutString("CUSTOM", 35), atts);
						addHeaderElement(mmDoc, "CodiceValore", FC_Utils.cutString(line.getProduct().getValue(), 35), atts);
					mmDoc.endElement("", "", "CodiceArticolo");
				}
				
				if ((line.getDescription() != null && line.getDescription().trim().length() > 0) 
						|| (line.get_ValueAsString("Name")!= null && line.get_ValueAsString("Name").trim().length() > 0)) {
					String desc = line.getDescription() != null ? line.getDescription() : "";
					String name = line.get_ValueAsString("Name") != null ? line.get_ValueAsString("Name") : "";
					
					String description = "";
					if(desc.length() > 0) {
						description += desc;
					}
					
					if(description.length() > 0 && name.length() > 0){
						description += " - ";
					}
					
					if(name.length() > 0) {
						description += name;
					}
					
					//sostituisco caratteri speciali non ammessi nella descrizione
					description = description.replaceAll("–", "-"); 
					
					addHeaderElement(mmDoc, "Descrizione", FC_Utils.cutString(description, 1000), atts);
				}
				else addHeaderElement(mmDoc, "Descrizione", " - ", atts);
				
				
				if (!(BigDecimal.ZERO.equals(line.getQtyEntered()) && BigDecimal.ZERO.equals(line.getPriceEntered()))) {
					addHeaderElement(mmDoc, "Quantita", FC_Utils.formatNumber(line.getQtyEntered(),"#0.00"), atts);
					String udm = DB.getSQLValueString(trxName, "select coalesce(ut.uomsymbol,u.uomsymbol) from c_uom u left join c_uom_trl ut on "
							+ "(u.c_uom_id=ut.c_uom_id and ad_language='it_IT') where u.c_uom_id=?", line.getC_UOM_ID());
					addHeaderElement(mmDoc, "UnitaMisura", FC_Utils.cutString(udm, 10), atts);
				}
				
				BigDecimal discount = line.get_Value("discount") != null ? (BigDecimal) line.get_Value("discount") : Env.ZERO;
				
				
				if (discount.compareTo(Env.ZERO) < 0) {
					throw new Exception("Errore: Sconto riga " + line.getLine() + " Negativo " );
				} else if (discount.abs().compareTo(new BigDecimal(100)) > 0) {
					throw new Exception("Errore: Sconto riga " + line.getLine() + " maggiore di 100 " );
				}
				
				//controllo che la riga della fattura sia con ritenuta INAIL, 
				//in tal caso nella riga va specificato il prezzo netto senza tener conto dello sconto
				int existsTrattenute = 0;
				if(line.get_ColumnIndex("TrattenutaLine_Ref_ID") >= 0) {
					String checkTrattenuteSQL = "SELECT count(*) from c_invoiceline where TrattenutaLine_Ref_ID = ? ";
					existsTrattenute = DB.getSQLValue(trxName, checkTrattenuteSQL, line.get_ID());
				}

				//se è specificato uno sconto si prende come prezzo unitario il prezzo di listino
				if (discount.abs().compareTo(Env.ZERO) > 0 && existsTrattenute <= 0) {
					
					addHeaderElement(mmDoc, "PrezzoUnitario", FC_Utils.formatNumber(line.getPriceList(),"#0.0000"), atts);
					
					mmDoc.startElement("","","ScontoMaggiorazione", atts);
					
					if (discount.compareTo(Env.ZERO) > 0) {
						addHeaderElement(mmDoc, "Tipo", "SC", atts);
					} else if (discount.compareTo(Env.ZERO) < 0) {
						addHeaderElement(mmDoc, "Tipo", "MG", atts);
					}
					addHeaderElement(mmDoc, "Percentuale", FC_Utils.formatNumber(discount,"#0.00"), atts);
					mmDoc.endElement("", "", "ScontoMaggiorazione");
				} else {
					addHeaderElement(mmDoc, "PrezzoUnitario", FC_Utils.formatNumber(line.getPriceEntered(),"#0.0000"), atts);
				}
				
				addHeaderElement(mmDoc, "PrezzoTotale", FC_Utils.formatNumber(line.getLineNetAmt(),"#0.00"), atts);
				//addHeaderElement(mmDoc, "AliquotaIVA", FC_Utils.formatNumber(tax.getRate(),"#0.00"), atts);

				//check if some tax child iswithholding in this case the AliquotaIVA is from the vat tax not from the withholding tax
				String queryCheckTaxChild = "select count(*) from c_tax where parent_tax_id = ? and iswithholding = 'Y'";
				boolean iswithholding =  false;
				BigDecimal taxRate = new  BigDecimal(0);

				if (DB.getSQLValue(trxName, queryCheckTaxChild, line.getC_Tax_ID()) > 0) {
					iswithholding = true;
					String queryCheckTaxChildRate = "select rate from c_tax where parent_tax_id = ? and iswithholding = 'N'";
					taxRate = DB.getSQLValueBD(trxName, queryCheckTaxChildRate, line.getC_Tax_ID());
				}


				String queryCheckTaxChildSplitPayment = "select count(*) from c_tax where parent_tax_id = ? and issplitpayment = 'Y'";
				boolean issplitpayment =  false;
				BigDecimal taxRateSP = new  BigDecimal(0);

				if (DB.getSQLValue(trxName, queryCheckTaxChildSplitPayment, line.getC_Tax_ID()) > 0) {
					issplitpayment = true;
					String queryCheckTaxChildRateSP = "select rate from c_tax where parent_tax_id = ? and issplitpayment = 'N'";
					taxRateSP = DB.getSQLValueBD(trxName, queryCheckTaxChildRateSP, line.getC_Tax_ID());
				}

				if (issplitpayment) {
					addHeaderElement(mmDoc, "AliquotaIVA", FC_Utils.formatNumber(taxRateSP,"#0.00"), atts);
				} else if (iswithholding) {
					addHeaderElement(mmDoc, "AliquotaIVA", FC_Utils.formatNumber(taxRate,"#0.00"), atts);
					addHeaderElement(mmDoc, "Ritenuta", "SI", atts);
				} else if (line.isDescription()){
					addHeaderElement(mmDoc, "AliquotaIVA", FC_Utils.formatNumber(twentytwo,"#0.00"), atts);
					isDescr = true;
				} else {
					taxRate = tax.getRate();
					addHeaderElement(mmDoc, "AliquotaIVA", FC_Utils.formatNumber(taxRate,"#0.00"), atts);
					if (BigDecimal.ZERO.equals(taxRate)) {
						String natura = DB.getSQLValueString(trxName, "select natura from c_tax where c_tax_id=?",
								tax.getC_Tax_ID());
						if (natura == null)
							throw new Exception("Errore: impostare il campo Natura per le esenzioni.");
						addHeaderElement(mmDoc, "Natura", natura, atts);
						
						//Imposto la dichiarazione di intento
						//if(natura.equals("N3.5")) {
						int fct_bp_esenzione_id = get_ValueAsInt("fct_bp_esenzione_id");
						if (fct_bp_esenzione_id > 0 && (natura.equals("N3.5"))) {
					
							mmDoc.startElement("","","AltriDatiGestionali", atts);
							
							X_fct_bp_esenzione esenzione = new X_fct_bp_esenzione(Env.getCtx(),fct_bp_esenzione_id,trxName);
							
							addHeaderElement(mmDoc, "TipoDato", "INTENTO", atts);
							addHeaderElement(mmDoc, "RiferimentoTesto", esenzione.getNumeroProtocollo(), atts);
							addHeaderElement(mmDoc, "RiferimentoData", FC_Utils.getDate(new Timestamp(esenzione.getDateRegistered().getTime()), 0), atts);
							
							mmDoc.endElement("","","AltriDatiGestionali");
						}
					}
				}	
				
				String rifAmministrazione = line.get_ValueAsString("RiferimentoAmministrazione");
				if(rifAmministrazione != null && !rifAmministrazione.isBlank()) {
					addHeaderElement(mmDoc, "RiferimentoAmministrazione", rifAmministrazione , atts);
				}
				
				mmDoc.endElement("", "", "DettaglioLinee");
			}	

			// Summary
			List<List<Object>> taxes = DB.getSQLArrayObjectsEx(trxName, 
					"select t.rate,sum(it.taxbaseamt),sum(it.taxamt), t.esigibilitaiva, t.natura, t.rifnormativo " + 
							"from c_invoicetax it "+ 
							"inner join c_tax t on (it.c_tax_id=t.c_tax_id) " + 
							"where  it.c_invoice_id=? and (t.iswithholding='N' and t.issplitpayment='N') " + 
							"group by t.rate, t.esigibilitaiva, t.natura, t.rifnormativo",get_ID());
			if (taxes == null)
				throw new Exception("Errore: manca il riepilogo iva, verificare le righe fattura.");
			for (int i =0;i<taxes.size();i++) {
				List<Object> taxLine = taxes.get(i);
				mmDoc.startElement("","","DatiRiepilogo", atts);
				addHeaderElement(mmDoc, "AliquotaIVA", FC_Utils.formatNumber(new BigDecimal(taxLine.get(0).toString()),"#0.00"), atts);
				if (twentytwo.compareTo((BigDecimal)taxLine.get(0)) == 0)
					found22 = true;
				if (taxLine.get(4) != null)	
					addHeaderElement(mmDoc, "Natura", taxLine.get(4)+"", atts);
				addHeaderElement(mmDoc, "ImponibileImporto", FC_Utils.formatNumber(new BigDecimal(taxLine.get(1).toString()),"#0.00"), atts);
				addHeaderElement(mmDoc, "Imposta", FC_Utils.formatNumber(new BigDecimal(taxLine.get(2).toString()),"#0.00"), atts);
				addHeaderElement(mmDoc, "EsigibilitaIVA", taxLine.get(3)+"", atts);
				if (taxLine.get(4) != null && taxLine.get(5) != null)
					addHeaderElement(mmDoc, "RiferimentoNormativo", FC_Utils.cutString(taxLine.get(5)+"",100), atts);
				mmDoc.endElement("", "", "DatiRiepilogo");
			}
			if (isDescr && !found22) {
				mmDoc.startElement("","","DatiRiepilogo", atts);
				addHeaderElement(mmDoc, "AliquotaIVA", FC_Utils.formatNumber(twentytwo,"#0.00"), atts);
				addHeaderElement(mmDoc, "ImponibileImporto", FC_Utils.formatNumber(BigDecimal.ZERO,"#0.00"), atts);
				addHeaderElement(mmDoc, "Imposta", FC_Utils.formatNumber(BigDecimal.ZERO,"#0.00"), atts);
				addHeaderElement(mmDoc, "EsigibilitaIVA", "I", atts);
				mmDoc.endElement("", "", "DatiRiepilogo");
			}
			mmDoc.endElement("", "", "DatiBeniServizi");

			// Payment
			MInvoicePaySchedule [] ips = MInvoicePaySchedule.getInvoicePaySchedule(getCtx(), get_ID(), 0, trxName); 
			if (ips != null && ips.length > 0) {
				String condPag = "TP01"; // more than 1 schedule of payment
				if (ips.length == 1) // just pay all once
					condPag = "TP02";

				mmDoc.startElement("","","DatiPagamento", atts);
				addHeaderElement(mmDoc, "CondizioniPagamento", condPag, atts);
				String payRule = DB.getSQLValueString(trxName, "select fc_modalitapagamento from c_invoice where c_invoice_id=?", getC_Invoice_ID());
				for (int i=0;i<ips.length;i++) {
					MInvoicePaySchedule ipsLine = ips[i];
					mmDoc.startElement("","","DettaglioPagamento", atts);
					addHeaderElement(mmDoc, "ModalitaPagamento", payRule, atts);
					addHeaderElement(mmDoc, "DataScadenzaPagamento", FC_Utils.getDate(ipsLine.getDueDate(),0), atts);
					addHeaderElement(mmDoc, "ImportoPagamento", FC_Utils.formatNumber(ipsLine.getDueAmt(),"#0.00"), atts);
					if ("MP05".equals(payRule)) { //bonifico
						String banca = "";
						String iban = "";
						int bankAccountID = DB.getSQLValue(trxName, "select c_bankaccount_id from c_bpartner where c_bpartner_id=?", getC_BPartner_ID());
						if (bankAccountID > 0) { // found on customer 
							iban = DB.getSQLValueString(trxName, "select iban from c_bankaccount where c_bankaccount_id=?", bankAccountID);
							banca = DB.getSQLValueString(trxName, "select b.name from c_bank b join c_bankaccount ba on (b.c_bank_id=ba.c_bank_id) where ba.c_bankaccount_id=?", bankAccountID);
						} else { // search for default
							banca = DB.getSQLValueString(trxName, "select name from c_bank where isownbank='Y' and isactive='Y' and ad_org_id in (?,0) order by created desc", getAD_Org_ID());
							iban = DB.getSQLValueString(trxName, "select iban from c_bankaccount ba join c_bank b on (b.c_bank_id=ba.c_bank_id) "
									+ "where b.isownbank='Y' and b.isactive='Y' and ba.isactive='Y' and ba.isdefault='Y' and b.ad_org_id in (?,0) order by b.created desc,ba.created desc", getAD_Org_ID());
						}
						if (banca!= null && !banca.isEmpty() && iban != null && !iban.isEmpty()) { 
							addHeaderElement(mmDoc, "IstitutoFinanziario", FC_Utils.cutString(banca,80), atts);
							addHeaderElement(mmDoc, "IBAN", iban, atts);
						}
					}
					mmDoc.endElement("", "", "DettaglioPagamento");
				}
				mmDoc.endElement("", "", "DatiPagamento");
			}
			mmDoc.endElement("", "", "FatturaElettronicaBody");
			mmDoc.endElement("", "", "p:FatturaElettronica");
			mmDoc.endDocument();

			if (mmDocStream != null) {
				mmDocStream.close();
			}

		} catch(Exception e) {
			e.printStackTrace();
			return e.getLocalizedMessage();
		}

		return "FILENAME" + file_name;
	}

	public void addHeaderElement(TransformerHandler mmDoc, String att, String value, AttributesImpl atts) throws Exception {
		if (att != null) {
			mmDoc.startElement("","",att,atts);
			mmDoc.characters(value.toCharArray(),0,value.toCharArray().length);
			mmDoc.endElement("","",att);
		} else {
			throw new AdempiereUserError(att + " empty");
		}
	}
	
	
	class CedentePrestatore {

		private String idPaese;
		private String idCodice;
		private String codiceFiscale;
		private String denominazione;
		private String regimeFiscale;
		private String indirizzo;
		private String provincia;
		private String cap;
		private String comune;
		private String nazione;
		private String riferimentoAmministrazione;
		
		CedentePrestatore() {
			
		}
		
		
		public CedentePrestatore( String idPaese, String idCodice, String codiceFiscale, String denominazione,
				String regimeFiscale, String indirizzo, String cap, String comune, String provincia, String nazione) {
			super();

			this.idPaese = idPaese;
			this.idCodice = idCodice;
			this.codiceFiscale = codiceFiscale;
			this.denominazione = denominazione;
			this.regimeFiscale = regimeFiscale;
			this.indirizzo = indirizzo;
			this.provincia = provincia;
			this.cap = cap;
			this.comune = comune;
			this.nazione = nazione;
		}
		
		

		public String getCodiceFiscale() {
			return codiceFiscale;
		}

		public void setCodiceFiscale(String codiceFiscale) {
			this.codiceFiscale = codiceFiscale;
		}

		public String getIdPaese() {
			return idPaese;
		}
		public void setIdPaese(String idPaese) {
			this.idPaese = idPaese;
		}
		public String getIdCodice() {
			return idCodice;
		}
		public void setIdCodice(String idCodice) {
			this.idCodice = idCodice;
		}
		public String getDenominazione() {
			return denominazione;
		}
		public void setDenominazione(String denominazione) {
			this.denominazione = denominazione;
		}
		public String getRegimeFiscale() {
			return regimeFiscale;
		}
		public void setRegimeFiscale(String regimeFiscale) {
			this.regimeFiscale = regimeFiscale;
		}
		public String getIndirizzo() {
			return indirizzo;
		}
		public void setIndirizzo(String indirizzo) {
			this.indirizzo = indirizzo;
		}
		public String getCap() {
			return cap;
		}
		public void setCap(String cap) {
			this.cap = cap;
		}
		public String getComune() {
			return comune;
		}
		public void setComune(String comune) {
			this.comune = comune;
		}
		public String getNazione() {
			return nazione;
		}
		public void setNazione(String nazione) {
			this.nazione = nazione;
		}
		public String getProvincia() {
			return provincia;
		}
		public void setProvincia(String provincia) {
			this.provincia = provincia;
		}


		public String getRiferimentoAmministrazione() {
			return riferimentoAmministrazione;
		}


		public void setRiferimentoAmministrazione(String riferimentoAmministrazione) {
			this.riferimentoAmministrazione = riferimentoAmministrazione;
		}

		
	}
	
	
	class CessionarioCommittente {
		

		private String idPaese;
		private String idCodice;
		private String codiceFiscale;
		private String denominazione;
		private String indirizzo;
		private String cap;
		private String comune;
		private String provincia;
		private String nazione;
		
		CessionarioCommittente() {
			
		}
	
		public CessionarioCommittente(String idPaese, String idCodice, String codiceFiscale, String denominazione,
				String indirizzo, String cap, String comune, String provincia, String nazione) {
			super();

			this.idPaese = idPaese;
			this.idCodice = idCodice;
			this.codiceFiscale = codiceFiscale;
			this.denominazione = denominazione;
			this.indirizzo = indirizzo;
			this.cap = cap;
			this.comune = comune;
			this.provincia = provincia;
			this.nazione = nazione;
		}
		
		
		public String getCodiceFiscale() {
			return codiceFiscale;
		}
		public void setCodiceFiscale(String codiceFiscale) {
			this.codiceFiscale = codiceFiscale;
		}
		public String getIdPaese() {
			return idPaese;
		}
		public void setIdPaese(String idPaese) {
			this.idPaese = idPaese;
		}
		public String getIdCodice() {
			return idCodice;
		}
		public void setIdCodice(String idCodice) {
			this.idCodice = idCodice;
		}
		public String getDenominazione() {
			return denominazione;
		}
		public void setDenominazione(String denominazione) {
			this.denominazione = denominazione;
		}
		public String getIndirizzo() {
			return indirizzo;
		}
		public void setIndirizzo(String indirizzo) {
			this.indirizzo = indirizzo;
		}
		public String getCap() {
			return cap;
		}
		public void setCap(String cap) {
			this.cap = cap;
		}
		public String getComune() {
			return comune;
		}
		public void setComune(String comune) {
			this.comune = comune;
		}
		public String getNazione() {
			return nazione;
		}
		public void setNazione(String nazione) {
			this.nazione = nazione;
		}
		public String getProvincia() {
			return provincia;
		}
		public void setProvincia(String provincia) {
			this.provincia = provincia;
		}
		
		
	}
		
		
	
}
