# Plugin per la fatturazione elettronica italiana su IDempiere

## Configurazione

- Accendendo al client System, finestra **Paese, Regione, Provincia**, Paese Italia e tab City, inserire le *città* dei clienti indicando nel campo *Locode* la sigla della provincia.
- Accedendo al client della azienda, nella finestra **Organizzazione** tab Informazioni Organizzazione inserire la *partita iva*.
- Nella stessa finestra, tab *Fatturazione Elettronica* inserire tutti i campi obbligatori: la ragione sociale senza caratteri speciali, tutti i dati relativi alla società e il path di dove salvare il file xml.
- Nella finestra **Business Partner**, al record del cliente, compilare la *partita iva* e l'*indirizzo PEC* se utilizzato per l'invio.
- Nella stessa finestra, tab Indirizzo verificare che sia inserita una città dal menu a tendina in modo che abbia una provincia collegata. E' obbligatorio anche il campo *CAP*.
- Nella stessa finestra, tab *Fatturazione elettronica*, indicare se il bp è PA oppure no e il codice destinatario se fornito oppure "0000000" se invio tramite PEC.
- Nella finestra **Fattura (Cliente)** inserendo la testata fattura specificare anche la *modalità di pagamento*.
- Dopo aver inserito la fattura con tutte le righe, per creare le scadenze è necessario completare la fattura.
- Infine, alla tab principale, dalla rotellina in alto a destra della toolbar, si può lanciare **Esporta Fattura in XML**.

## NOTA:
Per le fatture PA con codici CIG e/o CUP è necessario rendere visibili ed editabili i campi *Data Ordine* e *Riferimento Ordine* nella finestra **Fattura (Cliente)** nella tab principale.